package com.aei.reversi.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DiscTest {

    private Disc subject;

    @Before
    public void setUp() {
        subject = Disc.DARK;
    }

    @Test
    public void toString_ofDark() {
        Assert.assertEquals("Disc is in dark", subject.toString(), Disc.DARK.toString());
    }
}