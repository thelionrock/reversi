package com.aei.reversi.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class UserInputsTest {

    private UserInputs subject;

    @Before
    public void setUp() {
        subject = new UserInputs();
    }

    @Test
    public void retrievePlayerInput() throws IOException {
        URL resource = UserInputs.class.getResource("/empty");
        System.setIn(resource.openStream());
        String input = subject.retrievePlayerInput();
        Assert.assertEquals("User input captured", input, "Pretend user input");
    }

    @Test
    public void retrievePlayerInput_streamClosed() throws IOException {
        URL resource = UserInputs.class.getResource("/empty");
        InputStream inputStream = resource.openStream();
        inputStream.close();
        System.setIn(inputStream);
        String input = subject.retrievePlayerInput();
        Assert.assertEquals("User input cannot be captured", input, "");
    }
}