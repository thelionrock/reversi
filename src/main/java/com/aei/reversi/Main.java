package com.aei.reversi;

import com.aei.reversi.service.ReversiService;
import com.aei.reversi.service.UserInputs;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        ReversiService.getInstance().start(
                8,
                8,
                Arrays.asList(28, 35),
                Arrays.asList(27, 36),
                new UserInputs());
    }
}
