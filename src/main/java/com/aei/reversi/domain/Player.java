package com.aei.reversi.domain;

import com.aei.reversi.service.UserInputs;

import java.util.*;
import java.util.function.Function;

public class Player {

    private Disc disc;
    private Board board;
    private UserInputs userInputs;

    public Player(Disc disc, Board board, UserInputs userInputs) {
        this.disc = disc;
        this.board = board;
        this.userInputs = userInputs;
    }

    public void move(String input) {
        Coordinate coordinate = locate(input);
        if (coordinate == null && board.remainingCoordinates() == 0) {
            return;
        }
        if (coordinate == null) {
            System.out.println("Invalid move. Please try again.");
            System.out.print("\nPlayer " + this + " move: ");
            input = userInputs.retrievePlayerInput();
            move(input);
        }
        List<Coordinate> straightLines = findStraightLines(coordinate, disc);
        if (straightLines.isEmpty()) {
            return;
        }
        straightLines.add(coordinate);
        for (Coordinate straightLine : straightLines) {
            straightLine.setDisc(disc);
        }
        board.place(straightLines.toArray(new Coordinate[0]));
    }

    private List<Coordinate> findStraightLines(Coordinate coordinate, Disc ownDisc) {
        List<Coordinate> matrix = board.getMatrix();
        Integer width = board.getWidth();
        int ownIndex = matrix.indexOf(coordinate);
        if (ownIndex < 0) {
            return Collections.emptyList();
        }
        List<Coordinate> straightLines = new ArrayList<>();

        captureOpponentDiscs(ownIndex, ownIndex - 1, iterator -> iterator - 1, ownDisc, straightLines);
        captureOpponentDiscs(ownIndex, ownIndex + 1, iterator -> iterator + 1, ownDisc, straightLines);
        captureOpponentDiscs(ownIndex, ownIndex - width, iterator -> iterator - width, ownDisc, straightLines);
        captureOpponentDiscs(ownIndex, ownIndex - width - 1, iterator -> iterator - width - 1, ownDisc, straightLines);
        captureOpponentDiscs(ownIndex, ownIndex - width + 1, iterator -> iterator - width + 1, ownDisc, straightLines);
        captureOpponentDiscs(ownIndex, ownIndex + width, iterator -> iterator + width, ownDisc, straightLines);
        captureOpponentDiscs(ownIndex, ownIndex + width - 1, iterator -> iterator + width - 1, ownDisc, straightLines);
        captureOpponentDiscs(ownIndex, ownIndex + width + 1, iterator -> iterator + width + 1, ownDisc, straightLines);

        return straightLines;
    }

    private boolean captureOpponentDiscs(int lastIndex, int targetIndex, Function<Integer, Integer> function,
                                         Disc ownDisc, List<Coordinate> straightLines) {
        List<Coordinate> matrix = board.getMatrix();
        Integer width = board.getWidth();
        if ((lastIndex % width) == (width - 1) && lastIndex / width == targetIndex / width && Math.abs(targetIndex - lastIndex) > 1) {
            return false;
        } else if (lastIndex % width == 0 && lastIndex / width == targetIndex / width && Math.abs(targetIndex - lastIndex) > 1) {
            return false;
        } else if (targetIndex % width == (width - 1) && lastIndex % width == 0) {
            return false;
        } else if (targetIndex % width == 0 && lastIndex % width == (width - 1)) {
            return false;
        }
        Coordinate target;
        try {
            target = matrix.get(targetIndex);
        } catch (IndexOutOfBoundsException ex) {
            return false;
        }
        if (!ownDisc.equals(target.getDisc()) && !Disc.NO_DISC.equals(target.getDisc())) {
            boolean captured = captureOpponentDiscs(targetIndex, function.apply(targetIndex), function, ownDisc, straightLines);
            if (captured) {
                straightLines.add(target);
            }
            return captured;
        }
        return ownDisc.equals(target.getDisc());
    }

    private Coordinate locate(String index) {
        if (Objects.isNull(index) || index.trim().isEmpty()) {
            return null;
        }
        Character xCoordinate;
        int yCoordinate;
        try {
            xCoordinate = index.charAt(0);
            yCoordinate = Integer.valueOf(index.substring(1));
            if (isInsideBoundary(xCoordinate, yCoordinate) &&
                    isAdjacentDiscExists(xCoordinate, yCoordinate) &&
                    isAvailable(xCoordinate, yCoordinate)) {
                return new Coordinate(xCoordinate, yCoordinate);
            }
        } catch (NumberFormatException ex) {}
        try {
            int lastIndex = index.length() - 1;
            xCoordinate = index.charAt(lastIndex);
            yCoordinate = Integer.valueOf(index.substring(0, lastIndex));
            if (isInsideBoundary(xCoordinate, yCoordinate) &&
                    isAdjacentDiscExists(xCoordinate, yCoordinate) &&
                    isAvailable(xCoordinate, yCoordinate)) {
                return new Coordinate(xCoordinate, yCoordinate);
            }
        } catch (NumberFormatException ex) {}
        return null;
    }

    private boolean isAvailable(char xCoordinate, int yCoordinate) {
        List<Coordinate> matrix = board.getMatrix();
        int ownIndex = matrix.indexOf(new Coordinate(xCoordinate, yCoordinate));
        Coordinate coordinate = matrix.get(ownIndex);
        return Disc.NO_DISC.equals(coordinate.getDisc());
    }

    private boolean isInsideBoundary(char xCoordinate, int yCoordinate) {
        return board.getAlphabets().contains(Character.toLowerCase(xCoordinate)) && yCoordinate <= board.getHeight();
    }

    private boolean isAdjacentDiscExists(char xCoordinate, int yCoordinate) {
        Coordinate target = new Coordinate(xCoordinate, yCoordinate);
        List<Coordinate> matrix = board.getMatrix();
        Integer width = board.getWidth();
        Integer height = board.getHeight();
        int ownIndex = matrix.indexOf(target);
        if (ownIndex < 0) {
            return false;
        }
        Integer leftIndex = (yCoordinate - 1) * width + Math.max((ownIndex % width) - 1, 0);
        Integer rightIndex = (yCoordinate - 1) * width + Math.min((ownIndex % width) + 1, width - 1);
        Integer upIndex = Math.max(ownIndex - width, 0);
        Integer downIndex = Math.min(ownIndex + width, (height * width) + (ownIndex % width) - width);
        Integer upLeftIndex = Math.max(leftIndex - width, 0);
        Integer upRightIndex = Math.max(rightIndex - width, 0);
        Integer downLeftIndex = Math.min(leftIndex + width, (ownIndex % width == 0) ? downIndex : downIndex - 1);
        Integer downRightIndex = Math.min(rightIndex + width, (ownIndex == (height * width) - 1) ? downIndex : downIndex + 1);

        Set<Coordinate> neighbor = new HashSet<>(Arrays.asList(
                leftIndex != ownIndex ? matrix.get(leftIndex) : target,
                rightIndex != ownIndex ? matrix.get(rightIndex) : target,
                upIndex != ownIndex ? matrix.get(upIndex) : target,
                downIndex != ownIndex ? matrix.get(downIndex) : target,
                upLeftIndex != ownIndex ? matrix.get(upLeftIndex) : target,
                upRightIndex != ownIndex ? matrix.get(upRightIndex) : target,
                downLeftIndex != ownIndex ? matrix.get(downLeftIndex) : target,
                downRightIndex != ownIndex ? matrix.get(downRightIndex) : target
        ));
        return neighbor.stream()
                .anyMatch(coordinate -> !Disc.NO_DISC.equals(coordinate.getDisc()));
    }

    public Disc getDisc() {
        return disc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return disc == player.disc &&
                Objects.equals(board, player.board);
    }

    @Override
    public int hashCode() {
        return Objects.hash(disc, board);
    }

    @Override
    public String toString() {
        return "'" + disc + "'";
    }
}
