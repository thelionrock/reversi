package com.aei.reversi.domain;

import java.util.Objects;

public class Coordinate implements Comparable<Coordinate> {

    private Disc disc = Disc.NO_DISC;
    private Integer y;
    private String x;

    public Coordinate(Character x, Integer y) {
        this.x = String.valueOf(x);
        this.y = y;
    }

    public void setDisc(Disc disc) {
        this.disc = disc;
    }

    public Disc getDisc() {
        return disc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return Objects.equals(y, that.y) &&
                Objects.equals(x, that.x);
    }

    @Override
    public int hashCode() {
        return Objects.hash(y, x);
    }

    @Override
    public String toString() {
        return disc.toString();
    }

    @Override
    public int compareTo(Coordinate o) {
        Integer compareY = Integer.compare(y, o.y);
        Integer compareX = Character.compare(x.charAt(0), o.x.charAt(0));
        return compareY < 0 ? -1 : compareY > 0 ? 1 : compareX < 0 ? -1 : compareX > 0 ? 1 : 0;
    }
}
