package com.aei.reversi.domain;

import java.util.*;

public class Board {

    private Integer width;
    private Integer height;
    private List<Integer> initialDarkIndices;
    private List<Integer> initialLightIndices;
    private List<Coordinate> matrix = Collections.emptyList();
    private List<Character> alphabetsPool = Arrays.asList(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    private List<Character> alphabets = Collections.emptyList();

    public Board(Integer width, Integer height, List<Integer> darkIndices, List<Integer> lightIndices) {
        Objects.requireNonNull(width);
        Objects.requireNonNull(height);

        if (width > 26) {
            throw new RuntimeException("Width cannot exceed 26");
        }

        this.width = width;
        this.height = height;

        initialDarkIndices = darkIndices;
        initialLightIndices = lightIndices;
    }

    public void initialize() {
        int total = height * width;
        matrix = new ArrayList<>(total);
        alphabets = new ArrayList<>(alphabetsPool.subList(0, width));
        for (int i = 0; i < total; i++) {
            Coordinate coordinate = new Coordinate(
                    alphabets.get(i % width),
                    Double.valueOf(Math.floor((i / width))).intValue() + 1);
            if (initialLightIndices.contains(i)) {
                coordinate.setDisc(Disc.LIGHT);
            } else if (initialDarkIndices.contains(i)) {
                coordinate.setDisc(Disc.DARK);
            }
            matrix.add(coordinate);
        }
    }

    public void place(Coordinate... coordinates) {
        for (Coordinate coordinate : coordinates) {
            matrix.set(matrix.indexOf(coordinate), coordinate);
        }
    }

    public Long remainingCoordinates() {
        return matrix.stream()
                .filter(coordinate -> Disc.NO_DISC.equals(coordinate.getDisc()))
                .count();
    }

    public Boolean isAllDiscsInOneSide() {
        return matrix.stream()
                .allMatch(coordinate -> Arrays.asList(Disc.NO_DISC, Disc.DARK).contains(coordinate.getDisc())) ||
                matrix.stream()
                        .allMatch(coordinate -> Arrays.asList(Disc.NO_DISC, Disc.LIGHT).contains(coordinate.getDisc()));
    }

    public Boolean isFull() {
        return matrix.stream()
                .noneMatch(coordinate -> Disc.NO_DISC.equals(coordinate.getDisc()));
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }

    public List<Character> getAlphabets() {
        return alphabets;
    }

    public List<Coordinate> getMatrix() {
        return matrix;
    }
}
